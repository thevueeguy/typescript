var person = /** @class */ (function () {
    function person(name, age, salary, sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    person.quicksort = function (personArray, field, order) {
        var temporaryArray = [].concat(personArray);
        if (temporaryArray.length <= 1) {
            return temporaryArray;
        }
        var pivot;
        pivot = temporaryArray[0][field];
        var pivotItems;
        pivotItems = temporaryArray[0];
        var left = [];
        var right = [];
        for (var i = 1; i < temporaryArray.length; i++) {
            if (order === 'asc') {
                temporaryArray[i][field] < pivot ? left.push(temporaryArray[i]) : right.push(temporaryArray[i]);
            }
            else {
                temporaryArray[i][field] < pivot ? right.push(temporaryArray[i]) : left.push(temporaryArray[i]);
            }
        }
        return person.quicksort(left, field, order).concat(pivotItems, person.quicksort(right, field, order));
    };
    return person;
}());
var person1 = new person('Rekha', 11, 50000, 'M');
var person2 = new person('Rajesh', 12, 40000, 'M');
var person3 = new person('Raju', 13, 30000, 'M');
var person4 = new person('Rani', 14, 20000, 'F');
var person5 = new person('Ravi', 15, 10000, 'F');
var personArray = [person1, person2, person3, person4, person5];
console.log(person.quicksort(personArray, 'age', 'desc'));
