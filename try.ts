class person
{
    name: string;
    age: number;
    salary: number;
    sex: string;

    constructor(name: string, age:number, salary:number, sex:string)
    {
      this.name = name;
      this.age = age;
      this.salary = salary;
      this.sex = sex;
    }

    static quicksort(personArray: person[], field:string, order:string): person[]
    {
        let temporaryArray: person[] = [].concat(personArray);

        if (temporaryArray.length <= 1) 
        {
            return temporaryArray;
        }

        let pivot: number | string; 
        pivot = temporaryArray[0][field];

        let pivotItems: person; 
        pivotItems = temporaryArray[0];

        let left: person[] = []; 
        let right: person[] = [];

        for (let i = 1; i < temporaryArray.length; i++) 
        {
            if(order==='asc')
            {
                temporaryArray[i][field] < pivot ? left.push(temporaryArray[i]) : right.push(temporaryArray[i]);
            }
            else
            {
                temporaryArray[i][field] < pivot ? right.push(temporaryArray[i]) : left.push(temporaryArray[i]);
            }
        }
        return person.quicksort(left,field,order).concat(pivotItems, person.quicksort(right,field,order));
    }
}

let person1: person = new person('Rekha',11,50000,'M');
let person2: person = new person('Rajesh',12,40000,'M');
let person3: person = new person('Raju',13,30000,'M');
let person4: person = new person('Rani',14,20000,'F');
let person5: person = new person('Ravi',15,10000,'F');

const personArray: person[] = [person1,person2,person3,person4,person5];

console.log(person.quicksort(personArray,'age','desc'));